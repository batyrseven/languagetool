FROM maven

RUN ls
COPY ./ /languagetool
RUN ls
WORKDIR /languagetool
RUN mvn install --also-make -DskipTests

WORKDIR /languagetool/languagetool-server
RUN mvn install --also-make -DskipTests
RUN mvn package --also-make -DskipTests
RUN ls

ENTRYPOINT  ls
