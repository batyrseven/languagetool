#Deployment process

All artifacts of the final product are attached and are inside the zip file. To achieve the same results as us, and create pipelines described higher you will need to follow the next steps:

##Heroku setup

1. As you may have noticed from description and .gitlab-ci.yml file in the languagetool folder our application is deployed on two servers for staging and for prod. To ruun this servers you need some machine.
2. Go to the heroku.com, and create yourself an account
3. Then open this link https://dashboard.heroku.com/apps and press new on the right top corner
4. Press create new app, name it any name you want, I will call it “staging-app”
5. Create one more app, called “production-app”
6. Go to the link https://dashboard.heroku.com/account and take a look at your key  It is on the bottom of the page, we will need to use it, so you may save it somewhere

##GitLab setup

1. Create account on GitLab if you don’t already have one
2. Create your own repository(naming and privacy is on your own pick.
3. Open your repository, and after go to the Settings/CI/CD link ( https://gitlab.com/{YOUR_PROJECT_NAME}/-/settings/ci_cd )
4. Open Variables field, and create three variables:
        HEROKU_API_KEY = Key from heroku I told you to write somewhere
        HEROKU_APP_TESTING = { STAGING_APP}(in my case  staging-app)
        HEROKU_APP_PRODUCTION = {PRODUCTION_APP} (in my case  production-app)

!! The names that you are assigning to the HEROKU_APP_TESTING and HEROKU_APP_PRODUCTION are 
basically the names of your heroku servers !!

6. Clone repository on your local machine
7. Copy all the data from languagetool to your repo
8. Push everything to the GitLab
9. Create dev branch and restrict pushing to master(allow only merge requests)

You may see more precise description with images in the final report.

